<?php

Route::get('/admin', function(){
    return view('admin.admin');
});

Route::get('/sobre', function(){
    return view('sobre');
});

Route::get('/', function () {
    return view('welcome');
});
